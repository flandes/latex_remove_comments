*python script to remove comments from latex files*  

The template .tex is inspired from the question at:   http://tex.stackexchange.com/questions/83663/utility-to-strip-comments-from-latex-source
I do not claim everything is original in this script, but I was not very satisfied with solution I found elsewhere.

Copyright (C) 2014 François P. Landes

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.  
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.  
You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.

**Please cite when appropriate**.  

