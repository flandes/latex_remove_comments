#! /usr/bin/env python
##
## Creative Commons 2014: Francois P. Landes
## This comes with NO WARRANTY 
##
## Aim:
## this program is intended to delete all comments in a given latex file.
##
## Usage:  
## python remove-comments.py   Your_laTeX_File.tex
##
import sys
print 'USAGE:\npython Latex-remove-comments.py   Your_laTeX_File.tex'

flag_Verbatim = False

filename=sys.argv[1]
fout=open(filename[:-4]+'_no-comment.tex', 'w')
with open(filename, 'r') as f:
    for line in f:
    
        if line[:10] == "\\begin{verbatim}"[:10]:
            flag_Verbatim = True
        if line[:10] == "\\end{verbatim}"[:10]:
            flag_Verbatim = False


        if flag_Verbatim == True:
            fout.write(line)
        else: 
            ## take out the white spaces at the beggining of each line, to see if it starts with a %
            output = line
            while output[0]==" ":
                output=output[1:]
            ## skip the lines that start with a % : they do not correspond to white lines, instead they coreespond to "nothing"
            if output[0]=='%':
                ## we do not write this line at all.
                pass
            else:
                output = line   ## keep the whitespaces in the common/"normal" lines.
                iterator=-1
                for character in line:
                    iterator=iterator+1
                    if character == '%':
                        if iterator>0:
                            previousChar = line[iterator-1]
                            if previousChar== "\\" : #or previousChar== "}" or previousChar=='$':
                                pass ## we omit to delete the lines after the \% characters.
                            else:
                                output = line[:iterator]+'\n'   ## we delete the end of the line, after the % character.
                                break   ## we stop searching through this line
                fout.write(output)  # we write the modified line
            
fout.close()

