
echo "removing comments"
python latex-remove-comments.py template.tex 

echo "compiling both tex files (needs pdflatex)"
pdflatex template.tex   > pdflatexlog1
pdflatex template_no-comment.tex  > pdflatexlog1

echo "comparing pdf files (needs diffpdf)"
diffpdf templa*.pdf

echo "end"

